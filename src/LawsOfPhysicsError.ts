export class LawsOfPhysicsError extends Error {
  constructor(message) {
    super(message);
  }
}
