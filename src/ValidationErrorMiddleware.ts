import { validationResult } from "express-validator";

//////
/// Generic middleware handler for validation errors
//////

// TODO: Find a better way to automatically include this *after* the validation middleware,
// but before logic
export const validationErrorMiddleware = function (req, res, next) {
  const validationResults = validationResult(req);

  // If there isn't any validation errors, nothing else to do here
  if (validationResults.isEmpty())
    return next();

  // HTTP Status code 400, bad request
  // Give the user back the validation errors and exit out
  res.status(400).send({
    errors: validationResults.array()
  });
};
