import express from "express"
import { matchedData, param } from "express-validator"
import chalk from "chalk"
import { supportedConversions } from "./conversions.js";
import { TemperatureService } from "./TemperatureService.js";
import { validationErrorMiddleware } from "./ValidationErrorMiddleware.js";
import { LawsOfPhysicsError } from "./LawsOfPhysicsError.js";

//////
/// App configuration
//////

const port = parseInt(process.env.APP_PORT || "8080")
const app = express();
// Good ol' library magic strings
app.set('json spaces', 2)

// TODO: Add environment variable to toggle this
app.enable('trust proxy')

app.use((req, res, next) => {
  let from = req.ip;
  if (req.ips && req.ips.length)
    from = req.ips.join(",")

  console.log(chalk.yellow(from), chalk.blue(req.method), "-", chalk.green(req.path))
  next()
})

//////
/// API/Route layer
//////

// TODO: Implement proper DI, if needed
const temperatureServiceInstance = new TemperatureService()

app.get("/api/temperature", (req, res) => {
  res.status(200).send({
    supportedTemperatureUnits: supportedConversions
  })
})

// Convert to all available temperature units
app.get('/api/temperature/:sourceUnitName/:degrees',
  [
    param("sourceUnitName")
      .trim()
      .toLowerCase()
      .isIn(supportedConversions)
      .withMessage(`Must be one of: [${supportedConversions.join(', ')}]`),
    param("degrees")
      .toFloat()
      .isNumeric()
      .withMessage("Must be a number")
  ],
  validationErrorMiddleware,
  (req, res) => {
    const { sourceUnitName, degrees } = matchedData(req) as {
      sourceUnitName: string,
      degrees: number
    }

    const results = temperatureServiceInstance.ConvertToAll(sourceUnitName, degrees)
    res.status(200).send(results)
  }
);

// Convert to requested temperature unit
app.get('/api/temperature/:sourceUnitName/:degrees/:destinationUnitName',
  [
    param("sourceUnitName")
      .trim()
      .toLowerCase()
      .isIn(supportedConversions)
      .withMessage(`Must be one of: [${supportedConversions.join(', ')}]`),
    param("degrees")
      .toFloat()
      .isNumeric()
      .withMessage("Must be a number"),
    param("destinationUnitName")
      .trim()
      .toLowerCase()
      .isIn(supportedConversions)
      .withMessage(`Must be one of: [${supportedConversions.join(', ')}]`),

  ],
  validationErrorMiddleware,
  (req, res) => {
    const { sourceUnitName, degrees, destinationUnitName } = matchedData(req) as {
      sourceUnitName: string,
      degrees: number,
      destinationUnitName: string
    }

    const results = temperatureServiceInstance.ConvertTo(sourceUnitName, degrees, destinationUnitName)
    res.status(200).send({
      temperature: results,
      unit: destinationUnitName
    })
  }
);

//////
/// Error handling / post-route validation
//////

// Handle app related exceptions directly
app.use((err, req, res, next) => {
  if (err instanceof LawsOfPhysicsError) {
    res.status(400).send({
      errors: [{
        "type": "temperature",
        "msg": "Temperature is not allowed to go below absolute 0 (0K) due to laws of physics"
      }]
    });
    return
  }

  throw err
})

app.listen(port, () => {
  console.log(chalk.green("Listening on port"), chalk.blue(port))
})