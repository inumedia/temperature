import { LawsOfPhysicsError } from "./LawsOfPhysicsError.js";
import { conversions } from "./conversions.js";

export class TemperatureService {
  private Round(value: number, roundToDigit: number = 2) {
    const multiplier = 10 ** roundToDigit;
    return Math.round(value * multiplier) / multiplier;
  }

  private ValidateLawsOfPhysics(kelvinTemperature: number) {
    if (kelvinTemperature < 0) {
      throw new LawsOfPhysicsError("Temperature can not go below absolute zero as that would violate the laws of physics")
    }
  }

  ConvertToAll(fromTemperatureUnit: string, fromTemperature: number) {
    const currentUnit = {} as Record<string, number>;
    // TODO: Refactor intermediary conversion and validation to API layer
    const intermediary = conversions[fromTemperatureUnit].from(this.Round(fromTemperature));
    this.ValidateLawsOfPhysics(intermediary)
    // Note: We explicitly convert *back* to the sourceUnit
    // to highlight any floating point arithmetic issues
    // Reduce down all conversions to a single object
    return Object.keys(conversions)
      .reduce((allConversions, currentUnitName) => {
        // Convert to the unit and store it
        const destinationDegrees = conversions[currentUnitName].to(intermediary);
        allConversions[currentUnitName] = this.Round(destinationDegrees);
        return allConversions;
      }, currentUnit) as Record<string, number>;
  }

  ConvertTo(fromTemperatureUnit: string, fromTemperature: number, toTemperatureUnit: string) {
    // TODO: Refactor intermediary conversion and validation to API layer
    const intermediary = conversions[fromTemperatureUnit].from(fromTemperature);
    this.ValidateLawsOfPhysics(intermediary)

    const destinationDegrees = conversions[toTemperatureUnit].to(intermediary);
    return this.Round(destinationDegrees);
  }
}
