export interface UnitConversion {
  // Convert *to* this unit from Kelvin
  to(value: number): number
  // Convert *from* this unit to Kelvin
  from(value: number): number
}

//////
/// Initialize temperature conversion table
//////
export const conversions = {} as Record<string, UnitConversion>;
// TODO: Direct conversions between celsius and fahrenheit would likely be more accurate
// than using an intermediary, due to floating point arithmetic.
// Determine if accuracy is imperitive.
conversions["celsius"] = {
  to(value) { return value - 273.15; },
  from(value) { return value + 273.15; }
};
conversions["fahrenheit"] = {
  to(value) { return (value * 9 / 5) - 459.67; },
  from(value) { return (value + 459.67) * 5 / 9; }
};
// For sanity and simplicity
conversions["kelvin"] = {
  to(value) { return value; },
  from(value) { return value; }
};
export const supportedConversions = Object.keys(conversions);
