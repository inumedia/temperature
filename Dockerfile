FROM node:20-alpine

WORKDIR /app
COPY build/src .

CMD ["node", "/app/main.js"]