# Super simple temperature conversion API

This application was built and tested using Node 20.10.0 LTS, using Typescript and Express.

**Caveats:**
* This implementation uses floating integers, and can have standard floating arithmetic rounding errors (0.1 + 0.2 != 0.3), but rounding is currently hard coded to 2 decimal places and tends to mask the majority of errors.

## Configuring and running

By default this application will listen on port 8080. This can be changed using the environment variable `APP_PORT`.

Steps for running locally:
* Install Node 20 LTS
* Ensure Node and NPM is accessible from local terminal (`node --version`, `npm --version`)
* Run `npm -i` to download missing dependencies
* Run `npm run start` to compile the TypeScript code into JavaScript and run the express server

## API Reference

### __GET__ /api/temperature

Returns a list of all supported temperature units

Example output:
```
{
  "supportedTemperatureUnits": [
    "celsius",
    "fahrenheit",
    "kelvin"
  ]
}
```

### __GET__ /api/temperature/`:sourceUnitName`/`:degrees`

* `:sourceUnitName` must be a unit of measurement for temperature found in `/api/temperature`
* `:degrees` must be a number

Converts the input `:degrees` using all available temperature conversions

Example output:
```
{
  "celsius": 100,
  "fahrenheit": 212,
  "kelvin": 373.15
}
```

### __GET__ /api/temperature/`:sourceUnitName`/`:degrees`/`:destinationUnitName`

* `:sourceUnitName` must be a unit of measurement for temperature found in `/api/temperature`
* `:degrees` must be a number
* `:destinationUnitName` must be a unit of measurement for temperature found in `/api/temperature`

Converts the input `:degrees` to the target `:destinationUnitName`

Example output:
```
{
  "temperature": 212,
  "unit": "fahrenheit"
}
```